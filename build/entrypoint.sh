#!/bin/bash

set -e

echo this is your command :  $1

if [ "$1" == "backup" ]
then
	cron -f

elif [ "$1" == "s3configure" ]
then
	s3cmd --configure

else
	exec $@
fi
