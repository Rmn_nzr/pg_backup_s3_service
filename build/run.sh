#!/bin/bash

set -e

. /etc/environment
COUNTER=$(($NUM_OF_DB-1))

touch ~/.pgpass
chmod 600 ~/.pgpass
while [[ $COUNTER -ge 0 ]]
do
    echo ${DB_HOSTS[$COUNTER]}:${DB_PORTS[$COUNTER]}:${DB_NAMES[$COUNTER]}:${DB_USERS[$COUNTER]}:${DB_PASSS[$COUNTER]} > ~/.pgpass
    DB_PATH=/backups/${DB_NAMES[$COUNTER]}
    mkdir -p $DB_PATH
    LAST_BACKUP=$(ls $DB_PATH)
    BACKUP=$DB_PATH/`date +%Y-%m-%d_%H:%M`.psql.gz
    echo creating backup $BACKUP
    pg_dump -d ${DB_NAMES[$COUNTER]} -U ${DB_USERS[$COUNTER]} -h ${DB_HOSTS[$COUNTER]} -p ${DB_PORTS[$COUNTER]} | gzip > $BACKUP
    COUNTER=$(($COUNTER-1))
    if [ $REMOTE == true ]
    then
	    echo uploading to $S3BUCKET$BACKUP
	    /usr/bin/s3cmd put $BACKUP $S3BUCKET$BACKUP
	    echo uploaded
    fi
    echo removing ${DB_PATH}/$LAST_BACKUP
    rm ${DB_PATH}/$LAST_BACKUP
done
rm ~/.pgpass
echo done
